﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace API.Controllers
{
    public class HomeController:ApiController
    {
        public JsonResult<List<Persona>> Get()
        {
            List<Persona> personas = new List<Persona>();
            using (MySqlConnection conexion = new MySqlConnection("server=127.0.0.1; database=pacien; Uid=root; pwd=;"))
            {
                conexion.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * from clientes", conexion))
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        personas.Add(new Persona
                        {
                            id = dr.GetInt32(0),
                            nombre = dr.GetString(1),
                            apellido_p = dr.GetString(2),
                            apellido_M = dr.GetString(3),
                            edad = dr.GetInt32(4)
                        });
                    }
                }
            }
            return Json(personas);
        }

        public JsonResult<Estado>Post(PersonaRequest persona_)
        {
        if (persona_ == null) return Json(new Estado { detalle = "Faltan parametros", estado = false });
            using(MySqlConnection conexion = new MySqlConnection("server=127.0.0.1; database=pacien; Uid=root; pwd=;"))
            {
                conexion.Open();
                using(MySqlCommand cmd = new MySqlCommand("insert into clientes values (@id,@nombre,@apellido_p,@apellido_M,@edad)", conexion))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@id", persona_.id);
                        cmd.Parameters.AddWithValue("@nombre", persona_.nombre);
                        cmd.Parameters.AddWithValue("@apellido_p", persona_.apellido_p);
                        cmd.Parameters.AddWithValue("@apellido_m", persona_.apellido_M);
                        cmd.Parameters.AddWithValue("@edad", persona_.edad);
                        cmd.ExecuteNonQuery();
                    }
                    catch(Exception ex)
                    {
                        return Json(new Estado { detalle = ex.Message, estado = false });
                    }
                }
            }
            return Json(new Estado { detalle = "", estado = true });
        }
    }
}