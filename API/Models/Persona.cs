﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace API.Models
{
    public class Persona
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido_p { get; set; }
        public string apellido_M { get; set; }
        public int edad { get; set; }
        
    }
    public class PersonaRequest
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string apellido_p { get; set; }
        public string apellido_M { get; set; }
        public int edad { get; set; }
    }
}